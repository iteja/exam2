<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    
    public function actionConsulta1a(){
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select(['dorsal', 'nombre']),
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider, 
            "campos"=>['dorsal', 'nombre'], 
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Los ciclistas cuya edad esta entre 25 y 35 años mostrando unicamente el dorsal y el nombre del ciclista.",
            "sql"=>"SELECT DISTINCT dorsal, nombre FROM ciclista ",
        ]);
    }
    
        public function actionConsulta2a(){
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->distinct(),
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider, 
            "campos"=>['dorsal'], 
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Las etapas no circulares mostrando solo el numero de etapa y la longitud de las mismas.",
            "sql"=>"SELECT DISTINCT dorsal, nombre FROM ciclista ",
        ]);
    }
    
        public function actionConsulta3a(){
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->distinct(),
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider, 
            "campos"=>['dorsal'], 
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Los puertos con altura mayor a 1500 metros figurando el nombre del puerto y el dorsal del ciclista que lo gano.",
            "sql"=>"SELECT DISTINCT dorsal, nombre FROM ciclista ",
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
