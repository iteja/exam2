<?php

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">
<div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-sm-12">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 1</h3>
                        <p>Los ciclistas cuya edad esta entre 25 y 35 años mostrando unicamente el dorsal y el nombre del ciclista.</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-12">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 2</h3>
                        <p>Las etapas no circulares mostrando solo el numero de etapa y la longitud de las mismas.</p>
                        <p>
                           <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-12">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 3</h3>
                        <p>Los puertos con altura mayor a 1500 metros figurando el nombre del puerto y el dorsal del ciclista que lo gano.</p>
                        <p>
                           <?= Html::a('Active Record', ['site/consulta3a'], ['class' => 'btn btn-primary'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            
        </div>

    </div>
</div>
